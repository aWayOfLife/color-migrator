// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyC7SA8rA97Wz56f4Nl4UC8mxuEb7czu0VE",
    authDomain: "color-migrator.firebaseapp.com",
    projectId: "color-migrator",
    storageBucket: "color-migrator.appspot.com",
    messagingSenderId: "40619929149",
    appId: "1:40619929149:web:5cb7117a1c6e4035a84454"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
