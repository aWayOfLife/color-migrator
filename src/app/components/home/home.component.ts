import { Component, OnInit } from '@angular/core';
import { element } from 'protractor';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  oldColorsText: string;
  newColorsText: string;

  oldColors = [
    '#A7ABAD',
    '#AAAAAA',
    '#FFFFFF',
    '#9C9FA3',
    '#9B9DA1',
    '#9FA1A5',
    '#909297',
    '#999999',
    '#949494',
    '#8C8C8C',
    '#6B6E75',
    '#686D72',
    '#6C7689',
    '#75787E',
    '#737373',
    '#8E9095',
    '#878D94',
    '#282B31',
    '#22252A',
    '#3A3E47',
    '#333333',
    '#3B3F47',
    '#4E5258',
    '#40444C',
    '#4F5257',
    '#1C1F1F',
    '#232D33',
    '#242D33',
    '#252D33',
    '#F6F6F6',
    '#F4F4F4',
    '#F1F1F1',
    '#F3F3F3',
    '#F0F0F0',
    '#F5F5F6',
    '#FAFAFA',
    '#EBECED',
    '#EAEAEA',
    '#E2E3E5',
    '#EFEFEF',
    '#E6E6E6',
    '#E4E4E4',
    '#E2E2E2',
    '#C7C7C7',
    '#CCCCCC',
    '#CECFD1',
    '#B5B6B9',
    '#DCDCDC',
    '#D8DDDE',
    '#D9DADB',
    '#D8D8DA',
    '#D4D4D4',
    '#DDDDDD',
    '#E8E8E8',
    '#979797',
    '#D8D8D8',
    '#7A8081',
    '#F8F8F8',
    '#E5E5E5',
    '#ECECED',
    '#CFCFCF',
    '#FBFBFB',
    '#F5F5F5',
    '#000000',
    '#3A3E46',
    '#4E5257',
    '#93959A',
    '#DADADA'
  ];
  newColors = [
    '#FAFAFA',
    '#F2F2F2',
    '#D9D9D9',
    '#B3B3B3',
    '#7F7F7F',
    '#4D4D4D',
    '#262626'
  ];
  mappedColors = [];

  constructor() { }

  ngOnInit(): void {
    // tslint:disable-next-line: no-shadowed-variable
    this.generateMapping();
  }

  generateMapping() {
    this.mappedColors = [];
    this.oldColors.sort();
    this.newColors.sort();
    this.oldColors.forEach(element => {
      this.mappedColors.push(
        {
          oldColor : element,
          newColor : this.getClosestColor(element, this.newColors)
        }
      );
    });
    console.log(this.mappedColors);
  }

  getClosestColor(color, listColors) {
    const diffList = [];
    // tslint:disable-next-line: no-shadowed-variable
    listColors.forEach(element => {
      const diff = this.getDiffColor(color, element);
      diffList.push(diff);
    });
    const minDiff = Math.min(...diffList);
    const mappedColor = this.newColors[diffList.indexOf(minDiff)];
    return mappedColor;
  }

  getDiffColor(cola, colb) {
    const a = this.hexToRgb(cola);
    const b = this.hexToRgb(colb);
    return Math.sqrt(Math.pow((a.r - b.r), 2) + Math.pow((a.g - b.g), 2) + Math.pow((a.b - b.b), 2));
  }

  hexToRgb(hex) {
      const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      return result ? {
          r: parseInt(result[1], 16),
          g: parseInt(result[2], 16),
          b: parseInt(result[3], 16)
      } : null;
  }

  onChangeEvent(event: any, isNew: boolean) {
    if (!event.target.value) {
      this.oldColors = [];
      this.newColors = [];
      this.mappedColors = [];
      return;
    }
    if (!isNew) {
      // assign to old
      const oldColors = event.target.value.replace(/\s/g, '' )
        .replace(/'/g, '').replace(/"/g, '').split(',');
      this.oldColors = oldColors;
    }
    if (isNew) {
      // assign to new
      const newColors = event.target.value.replace(/\s/g, '' )
        .replace(/'/g, '').replace(/"/g, '').split(',');
      this.newColors = newColors;
    }
    // calculate
    this.generateMapping();
  }

}
